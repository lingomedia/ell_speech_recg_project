# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SpeechRecgConfig(AppConfig):
    name = 'speech_recg'
