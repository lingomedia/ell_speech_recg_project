import os
import sys
from pocketsphinx import get_model_path, get_data_path, Pocketsphinx, AudioFile

phrase = sys.argv[1]
decode_file_name = sys.argv[2]

model_path = get_model_path()
data_path = "./"

config = {
    'lm': False,
    'hmm': os.path.join(model_path, 'en-us'),
    'keyphrase': phrase,
    'kws_threshold': 1e-50,
    'dict': os.path.join(model_path, 'cmudict-en-us.dict'),
    'audio_file': os.path.join(data_path, decode_file_name),
    'buffer_size': 4096,
    'no_search': False,
    'full_utt': False
}

audio = AudioFile(**config)
for phrase in audio:
    print(phrase.segments(detailed=True))
