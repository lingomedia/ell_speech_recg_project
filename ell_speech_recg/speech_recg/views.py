# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from pydub import AudioSegment
import os
import subprocess
import time


@csrf_exempt
def recognize_speech(request):
    if request.method == "POST":
        recordingID = request.POST['recordingID']
        print "recordingID: " + recordingID
        print "Phrase: " + request.POST['phrase']

        uploadedFileName = "recording_"+recordingID + ".wav"
        uploadedFile = open(uploadedFileName, "wb")
        uploadedFile.write(request.FILES['soundblob'].read())
        uploadedFile.close()

        decodedFileName = "decoded_"+recordingID + ".wav"
        wav_audio = AudioSegment.from_file(uploadedFileName, format="wav")
        print "Audio bytes: " + str(wav_audio.sample_width)
        print "Audio sample rate: " + str(wav_audio.frame_rate)
        print "Audio dBFS: " + str(wav_audio.dBFS)
        wav_audio_new = wav_audio
        wav_audio_new = wav_audio_new.set_channels(1)
        wav_audio_new = wav_audio_new.set_frame_rate(16000)
        #wav_audio_new = wav_audio_new.apply_gain(3)
        wav_audio_new = match_target_amplitude(wav_audio_new, -40)
        wav_audio_new = wav_audio_new.set_sample_width(2)
        wav_audio_new.export(decodedFileName, format='wav')

        time.sleep(3)

        #We need to use subprocess because Django has some conflict with PocketSphinx
        recognized_text = subprocess.check_output(["python", "speech_recg/recognize_recording.py", request.POST['phrase'], decodedFileName])

        print recognized_text

        os.remove(uploadedFileName)
        os.remove(decodedFileName)

        return HttpResponse(recognized_text)
    elif request.method == "GET":
        print "Got get request"
        return HttpResponse("Post some audio to me!")
    else:
        return HttpResponse("Not sure what you wanted.")


def match_target_amplitude(sound, target_dBFS):
    change_in_dBFS = target_dBFS - sound.dBFS
    return sound.apply_gain(change_in_dBFS)
